module.exports = {
    TOKEN: "",
    DEFAULT_COOLDOWN: 3,
    DEFAULT_CLEAR_CHAT: 10,
    DBCONNECTION: "",
    DEFAULTSETTING: {
        prefix: "?",
        logChannel: "bot-logs",
        clearChat: 10,
        cooldown: 3,
    }
}
